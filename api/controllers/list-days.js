"use strict";
const Day = require("../models/day");

module.exports = async (req, res) => {
  try {
    const docs = await Day.find({ user: req.userData.userId })
      .select({ _id: 1, break: 1, in: 1, out: 1 }).exec();
    if (docs.length >= 0) {
      res.status(200).json(docs);
    } else {
      res.status(200).json({
        message: "No documents found"
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
