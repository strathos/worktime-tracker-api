"use strict";
const moment = require("moment");
const Day = require("../models/day");

module.exports = async (req, res) => {
  try {
    let start;
    const weekMatch = new RegExp("^[0-5][0-9]$");
    const yearWeekMatch = new RegExp("^[2][0][0-9][0-9][0,\-,W][0-9][0-9]$");
    if (req.params.week === "this") {
      start = moment().startOf("isoWeek").format("YYYY-MM-DD");
    } else if (req.params.week === "last") {
      start = moment().startOf("isoWeek").subtract(1, "week").format("YYYY-MM-DD");
    } else if (weekMatch.test(req.params.week)) {
      start = moment().week(req.params.week).startOf("isoWeek").format("YYYY-MM-DD");
    } else if (yearWeekMatch.test(req.params.week)) {
      start = moment(req.params.week).startOf("isoWeek").format("YYYY-MM-DD");
    } else {
      start = moment().startOf("isoWeek").format("YYYY-MM-DD");
    }
    const end = moment(start).add(1, "w").format("YYYY-MM-DD");
    const doc = await Day.find({
      $or: [
        {
          in: { $gte: start, $lte: end }
        },
        {
          out: { $gte: start, $lte: end }
        }
      ],
      user: req.userData.userId
    }).select({ _id: 1, break: 1, in: 1, out: 1 }).exec();
    if (doc) {
      res.status(200).json(doc);
    } else {
      res.status(404).json({
        message: "Date not found"
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
