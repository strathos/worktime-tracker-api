const request = require("supertest");
const app = require("../api/app");

afterAll(async () => {
  // avoid jest open handle error
  await new Promise(resolve => setTimeout(() => resolve(), 500));
});

describe("Unauthenticated", () => {
  test("It should respond with 404 from root", async () => {
    const response = await request(app).get("/");
    expect(response.statusCode).toBe(404);
  });

  test("It should respond with 401 with bad id and token", async () => {
    const response = await request(app).get("/user/forgot/asd/qwerty");
    expect(response.statusCode).toBe(401);
  });

  test("It should respond with 401 without token", async () => {
    const response = await request(app).get("/days");
    expect(response.statusCode).toBe(401);
  });

  test("It should respond with 401 with invalid token", async () => {
    const response = await request(app)
      .get("/days")
      .set("Authorization", "Bearer 123")
      .send();
    expect(response.text).toEqual('{"error":"Authorization failed"}');
    expect(response.statusCode).toBe(401);
  });
});

describe("Authenticated", () => {
  let token = "";
  let dayId = "";
  let userId = "";

  test("It should create a user", async () => {
    const response = await request(app)
      .post("/user/signup")
      .set("Content-type", "application/json")
      .send({
        "email": "tester@test.com",
        "password": "tester"
      });
    expect(response.text).toEqual('{"message":"User created"}');
    expect(response.statusCode).toBe(201);
  });

  test("It should fail with same email", async () => {
    const response = await request(app)
      .post("/user/signup")
      .set("Content-type", "application/json")
      .send({
        "email": "tester@test.com",
        "password": "tester"
      });
    expect(response.text).toEqual('{"error":"Already exists"}');
    expect(response.statusCode).toBe(409);
  });

  test("It should successfully login", async () => {
    const response = await request(app)
      .post("/user/login")
      .set("Content-type", "application/json")
      .send({
        "email": "tester@test.com",
        "password": "tester"
      });
    token = response.body.token;
    expect(response.body.message).toEqual("Authorization successful");
    expect(response.statusCode).toBe(200);
  });

  test("It should successfully answer it user ID", async () => {
    const response = await request(app)
      .get("/user")
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send();
    userId = response.body.userId;
    expect(response.body.message).toEqual("Authorized");
    expect(response.statusCode).toBe(200);
  });

  test("It should successfully create a day", async () => {
    const response = await request(app)
      .post("/days")
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send({
        "in": "2019-08-01T08:00:00+03:00",
        "out": "2019-08-01T16:00:00+03:00",
        "break": "true"
      });
    dayId = response.body.day.id;
    expect(response.body.message).toEqual("Day created");
    expect(response.statusCode).toBe(201);
  });

  test("It should successfully patch a day", async () => {
    const response = await request(app)
      .patch(`/days/${dayId}`)
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send({
        "break": "false"
      });
    expect(response.text).toEqual(`{"break":false,"_id":"${dayId}","in":"2019-08-01T05:00:00.000Z","out":"2019-08-01T13:00:00.000Z"}`);
    expect(response.statusCode).toBe(200);
  });

  test("It should list one day", async () => {
    const response = await request(app)
      .get("/days/date/2019-08-01")
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send();
    expect(response.text).toEqual(`{"break":false,"_id":"${dayId}","in":"2019-08-01T05:00:00.000Z","out":"2019-08-01T13:00:00.000Z"}`);
    expect(response.statusCode).toBe(200);
  });

  test("It should fail to list non-created day", async () => {
    const response = await request(app)
      .get("/days/date/2019-08-02")
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send();
    expect(response.body.message).toEqual("Date not found");
    expect(response.statusCode).toBe(404);
  });

  test("It should successfully delete a day", async () => {
    const response = await request(app)
      .delete(`/days/${dayId}`)
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send();
    expect(response.body.message).toEqual("Day deleted");
    expect(response.statusCode).toBe(200);
  });

  test("It should fail to list the deleted day", async () => {
    const response = await request(app)
      .get("/days/date/2019-08-01")
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send();
    expect(response.body.message).toEqual("Date not found");
    expect(response.statusCode).toBe(404);
  });

  test("It should successfully delete the user", async () => {
    const response = await request(app)
      .delete(`/user/${userId}`)
      .set("Content-type", "application/json")
      .set('Authorization', `Bearer ${token}`)
      .send();
    expect(response.body.message).toEqual("User deleted");
    expect(response.statusCode).toBe(200);
  });

  test("It should fail to login after user was deleted", async () => {
    const response = await request(app)
      .post("/user/login")
      .set("Content-type", "application/json")
      .send({
        "email": "tester@test.com",
        "password": "tester"
      });
    expect(response.body.error).toEqual("Authorization failed");
    expect(response.statusCode).toBe(401);
  });

});
