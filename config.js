"use strict";
require('dotenv').config();
const port = process.env.PORT || 3000;
const mongodbUsername = process.env.MONGODB_USER || false;
const mongodbPassword = process.env.MONGODB_PASS || false;
const mongodbUri = process.env.MONGODB_URI || "localhost:27017";
let mongodbPrefix;

if (mongodbUsername && mongodbPassword) {
  mongodbPrefix = "mongodb://" + mongodbUsername + ":" + mongodbPassword + "@";
} else {
  mongodbPrefix = "mongodb://";
}

module.exports.env = process.env.NODE_ENV || "PROD";
module.exports.jwt = {
  key: process.env.JWT_KEY || "secret",
  expiry: process.env.JWT_EXPIRY || "1h"
};

module.exports.server = {
  api_url: process.env.API_URL || "http://localhost:" + port,
  port
};

module.exports.mongodb = {
  username: mongodbUsername,
  password: mongodbPassword,
  uri: mongodbPrefix + mongodbUri
};

module.exports.smtp = {
  host: process.env.SMTP_HOST || null,
  port: process.env.SMTP_PORT || null,
  secure: process.env.SMTP_SECURE || false,
  tls: process.env.SMTP_TLS || null,
  user: process.env.SMTP_USER || null,
  pass: process.env.SMTP_PASS || null,
  fromName: process.env.SMTP_FROM_NAME || null,
  fromAddress: process.env.SMTP_FROM_ADDRESS || null
};
