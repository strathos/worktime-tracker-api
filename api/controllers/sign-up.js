"use strict";
const User = require("../models/user");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

module.exports = async (req, res) => {
  try {
    const user = await User.find({ email: req.body.email }).exec();
    if (user.length >= 1) {
      return res.status(409).json({
        error: "Already exists"
      });
    } else {
      bcrypt.hash(req.body.password, 10, async (err, hash) => {
        if (err) {
          return res.status(500).json({
            error: err
          });
        } else {
          const user = new User({
            _id: new mongoose.Types.ObjectId(),
            email: req.body.email,
            password: hash
          });
          await user.save();
          res.status(201).json({
            message: "User created"
          });
        }
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
