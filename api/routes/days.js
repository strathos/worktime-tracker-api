"use strict";
const express = require("express");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const listDays = require("../controllers/list-days");
const dayById = require("../controllers/get-day-by-id");
const dayByDate = require("../controllers/get-day-by-date");
const daysByWeek = require("../controllers/get-days-by-week");
const createDay = require("../controllers/create-day");
const updateDay = require("../controllers/update-day");
const deleteDay = require("../controllers/delete-day");

router.get("/", checkAuth, listDays);
router.get("/id/:id", checkAuth, dayById);
router.get("/date/:date", checkAuth, dayByDate);
router.get("/week/:week", checkAuth, daysByWeek);
router.post("/", checkAuth, createDay);
router.patch("/:id", checkAuth, updateDay);
router.delete("/:id", checkAuth, deleteDay);

module.exports = router;
