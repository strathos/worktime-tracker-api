"use strict";
const Day = require("../models/day");

module.exports = async (req, res) => {
  try {
    const doc = await Day.findOne({ _id: req.params.id, user: req.userData.userId })
      .select({ _id: 1, break: 1, in: 1, out: 1 }).exec();
    if (doc) {
      res.status(200).json(doc);
    } else {
      res.status(404).json({
        message: "Id not found"
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
