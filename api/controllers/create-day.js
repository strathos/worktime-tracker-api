"use strict";
const Day = require("../models/day");
const mongoose = require("mongoose");

module.exports = async (req, res) => {
  const day = new Day({
    _id: new mongoose.Types.ObjectId(),
    in: req.body.in,
    out: req.body.out,
    break: req.body.break,
    user: req.userData.userId
  });
  try {
    const newDay = await day.save();
    let doc = { id: newDay.id };
    if (newDay.in) doc.in = newDay.in;
    if (newDay.out) doc.out = newDay.out;
    if (newDay.break) doc.break = newDay.break;
    res.status(201).json({
      message: "Day created",
      day: doc
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
