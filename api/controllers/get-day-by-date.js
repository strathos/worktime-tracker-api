"use strict";
const moment = require("moment");
const Day = require("../models/day");

module.exports = async (req, res) => {
  try {
    let start;
    if (req.params.date === "today") {
      start = moment().format("YYYY-MM-DD");
    } else {
      start = moment(req.params.date).format("YYYY-MM-DD");
    }
    const end = moment(start).add(1, "d").format("YYYY-MM-DD");
    console.log(start, end);
    const doc = await Day.findOne({
      $or: [
        {
          in: { $gte: start, $lte: end }
        },
        {
          out: { $gte: start, $lte: end }
        }
      ],
      user: req.userData.userId
    }).select({ _id: 1, break: 1, in: 1, out: 1 }).exec();
    if (doc) {
      res.status(200).json(doc);
    } else {
      res.status(404).json({
        message: "Date not found"
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
