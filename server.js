"use strict";
const config = require("./config");
const http = require("http");
const app = require("./api/app");
const server = http.createServer(app);

server.listen(config.server.port, err => {
  if (err) {
    console.error(err);
  } else {
    console.log("API Server started, listening on port:", config.server.port);
    console.log("Configured API URL:", config.server.api_url);
  }
});
