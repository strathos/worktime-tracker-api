"use strict";
const express = require("express");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const checkUserStatus = require("../controllers/check-user-status");
const signUp = require("../controllers/sign-up");
const changePw = require("../controllers/change-pw");
const deleteUser = require("../controllers/delete-user");
const login = require("../controllers/login");
const forgotPwPageGet = require("../controllers/forgot-pw-page-get");
const forgotPwPagePost = require("../controllers/forgot-pw-page-post");
const forgotPw = require("../controllers/forgot-pw");

router.get("/", checkAuth, checkUserStatus);
router.patch("/", changePw);
router.post("/signup", signUp);
router.delete("/:id", checkAuth, deleteUser);
router.post("/login", login);
router.get("/forgot/:id/:token", forgotPwPageGet);
router.post("/forgot/:id/:token", forgotPwPagePost);
router.post("/forgot", forgotPw);

module.exports = router;
