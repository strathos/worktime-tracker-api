"use strict";
const Day = require("../models/day");

module.exports = async (req, res) => {
  const id = req.params.id;
  try {
    const obj = await Day.deleteOne({ _id: id, user: req.userData.userId }).exec();
    if (obj.n === 0) {
      res.status(200).json({
        message: "Day not found"
      });
    } else {
      res.status(200).json({
        message: "Day deleted"
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
