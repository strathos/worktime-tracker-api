"use strict";
const User = require("../models/user");
const bcrypt = require("bcrypt");

module.exports = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email }).exec();
    if (user == null) {
      return res.status(401).json({
        error: "Authorization failed"
      });
    }
    bcrypt.compare(req.body.password, user.password, async (err, result) => {
      if (err) {
        console.error(err);
        return res.status(401).json({
          error: "Authorization failed"
        });
      }
      if (result) {
        bcrypt.hash(req.body.newpassword, 10, async (err, hash) => {
          if (err) {
            return res.status(500).json({
              error: err
            });
          } else {
            try {
              await User.findOneAndUpdate({ _id: user.id }, { $set: { password: hash } }).exec();
              return res.status(200).json({
                message: "Password changed successfully"
              });
            } catch (err) {
              console.error(err);
              res.status(500).json({
                error: err
              });
            }
          }
        });
      } else {
        res.status(401).json({
          error: "Authorization failed"
        });
      }
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
