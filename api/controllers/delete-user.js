"use strict";
const Day = require("../models/day");
const User = require("../models/user");

module.exports = async (req, res) => {
  if (req.params.id == req.userData.userId) {
    try {
      const obj = await User.deleteOne({ _id: req.params.id }).exec();
      if (obj.n === 0) {
        res.status(200).json({
          message: "User not found"
        });
      } else {
        await Day.deleteMany({ user: req.params.id }).exec();
        res.status(200).json({
          message: "User deleted"
        });
      }
    } catch (err) {
      console.error(err);
      res.status(500).json({
        error: err
      });
    }
  } else {
    res.status(401).json({
      error: "Authorization failed"
    });
  }
};
