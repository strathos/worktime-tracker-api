"use strict";
const User = require("../models/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const config = require("../../config");

module.exports = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email }).exec();
    if (user == null) {
      return res.status(401).json({
        error: "Authorization failed"
      });
    }
    bcrypt.compare(req.body.password, user.password, async (err, result) => {
      if (err) {
        console.error(err);
        return res.status(401).json({
          error: "Authorization failed"
        });
      }
      if (result) {
        const token = await jwt.sign(
          {
            email: user.email,
            userId: user._id
          },
          config.jwt.key,
          {
            expiresIn: config.jwt.expiry
          }
        );
        return res.status(200).json({
          message: "Authorization successful",
          token: token
        });
      }
      res.status(401).json({
        error: "Authorization failed"
      });
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
