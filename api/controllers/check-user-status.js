"use strict";
module.exports = (req, res) => {
  res.status(200).json({
    message: "Authorized",
    email: req.userData.email,
    userId: req.userData.userId
  });
};
