"use strict";
const Day = require("../models/day");

module.exports = async (req, res) => {
  let doc = {};
  if (req.body.in) doc.in = req.body.in;
  if (req.body.out) doc.out = req.body.out;
  if (req.body.break.toString) doc.break = req.body.break;
  try {
    const result = await Day.findOneAndUpdate({ _id: req.params.id, user: req.userData.userId }, { $set: doc }, { new: true })
      .select({ _id: 1, break: 1, in: 1, out: 1 }).exec();
    if (result === null) {
      res.status(200).json({
        message: "User not found"
      });
    } else {
      res.status(200).json(result);
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
