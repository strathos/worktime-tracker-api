"use strict";
const jwt = require("jsonwebtoken");
const nodemailer = require("nodemailer");
const User = require("../models/user");
const config = require("../../config");

let options = {
  host: config.smtp.host,
  port: config.smtp.port,
  auth: {
    user: config.smtp.user,
    pass: config.smtp.pass
  }
};

if (config.smtp.secure.toString) options.secureConnection = config.smtp.secure;
if (config.smtp.tls) options.tls = JSON.parse(config.smtp.tls);

const transporter = nodemailer.createTransport(options);

module.exports = async (req, res) => {
  try {
    const user = await User.findOne({ email: req.body.email }).exec();
    if (user == null) {
      return res.status(200).json({
        message: "Email may have been sent"
      });
    } else {
      const token = await jwt.sign(
        {
          userId: user._id
        },
        user.password,
        {
          expiresIn: 3000
        }
      );
      let info = await transporter.sendMail({
        from: `"${config.smtp.fromName}" <${config.smtp.fromAddress}>`,
        to: req.body.email,
        subject: "Reset your Worktime Tracker password",
        text: `Reset your password here: ${config.server.api_url}/user/forgot/${user._id}/${token}`,
        html: `Reset your password here: <a href=${config.server.api_url}/user/forgot/${user._id}/${token}>Go to reset page</a>`
      });
      if (config.env === "DEV") {
        console.log("Message sent: %s", info.messageId);
        console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      }
      return res.status(200).json({
        message: "Email may have been sent"
      });
    }
  } catch (err) {
    console.error(err);
    res.status(500).json({
      error: err
    });
  }
};
