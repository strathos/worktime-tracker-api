# Node.js REST API server for worktime tracker
## Routes
* GET /user/
* PATCH /user/
* POST /user/signup
* POST /user/login
* DELETE /user/:id
* GET /days/
* GET /days/id/:id
* GET /days/date/:date
* GET /days/date/today
* GET /days/week/:week
* GET /days/week/this
* GET /days/week/last
* POST /days/
* PATCH /days/:id
* DELETE /days/:id
## Configuration
Configuration is done fully with environment variables.
### List of variables
* PORT (default 3000)
* NODE_ENV (default PROD)
* MONGODB_USER (default false)
* MONGODB_PASS (default false)
* MONGODB_URI (default localhost:27017)
* JWT_KEY (default secret)
* JWT_EXPIRY (default 1h)
## Example nodemon.json
```json
{
  "env": {
    "NODE_ENV": "DEV",
    "PORT": 4000,
    "MONGODB_USER": "worktime-tracker-api",
    "MONGODB_PASS": "p4$5W0rD",
    "MONGODB_URI": "remote-host:27017",
    "JWT_KEY": "WANjpHU4ZWgxpkJ6us4fUBpVKmn47QMM",
    "JWT_EXPIRY": "1d"
  }
}
```
## Example .env file
```bash
PORT=4000
NODE_ENV=DEV
MONGODB_USER=worktime-tracker-api
MONGODB_PASS=PaSsWoRd
MONGODB_URI=localhost:27017
```
## Example http file for VSCode Rest Client

```
@host = http://localhost:4000
@token = eyJhbGciOASDnR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImthcmlAdGVzdC5jb20iLCASDSWQiOiI1ZDQ0ODlkOTRlZjQ0YzAwMTk4Yzg3ZGYiLCJpYXQiOjE1NjQ4MzAzasd4cCI6MTU2NDkxNjczOX0.0QDFrpAwkrF1g8BasdRzWyGBSed92AYVAGI

###

POST {{ host }}/user/signup
content-type: application/json

{
  "email": "tester1@test.com",
  "password": "tester"
}

###

POST {{ host }}/user/login
content-type: application/json

{
  "email": "tester1@test.com",
  "password": "tester"
}

###

PATCH {{ host }}/user/
content-type: application/json

{
  "email": "tester1@test.com",
  "password": "tester",
  "newpassword": "test"
}

###

GET {{ host }}/days
Authorization: Bearer {{ token }}

###

GET {{ host }}/days/date/2018-02-13
Authorization: Bearer {{ token }}

###

GET {{ host }}/days/week/last
Authorization: Bearer {{ token }}

###

GET {{ host }}/days/id/5a817b8b6b2b262b6cc776f4
Authorization: Bearer {{ token }}

###

POST {{ host }}/days
content-type: application/json
Authorization: Bearer {{ token }}

{
  "in": "2018-02-11T13:34:19+00:00",
  "break": "false"
}

###

PATCH {{ host }}/days/5a818866da0ce741c403bd62
content-type: application/json
Authorization: Bearer {{ token }}

{
  "in": "2018-02-11T12:24:19+00:00",
  "out": "2018-02-11T16:44:19+00:00",
  "break": "true"
}

###

GET {{ host }}/user/
Authorization: Bearer {{ token }}

###

DELETE {{ host }}/user/5a818761524c2e42d431a3a9
Authorization: Bearer {{ token }}

```
