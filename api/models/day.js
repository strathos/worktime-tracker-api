"use strict";
const mongoose = require("mongoose");

const daySchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  in: {
    type: Date,
    required: false
  },
  out: {
    type: Date,
    required: false
  },
  break: {
    type: Boolean,
    default: true,
    required: false
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true
  }
});

module.exports = mongoose.model("Day", daySchema);
