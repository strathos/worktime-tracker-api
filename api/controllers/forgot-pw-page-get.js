"use strict";
const jwt = require("jsonwebtoken");
const User = require("../models/user");

module.exports = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id }).exec();
    if (user == null) {
      return res.status(401).json({
        error: "Authorization failed"
      });
    } else {
      const token = req.params.token;
      try {
        await jwt.verify(token, user.password);
        return res.render("forgot-pw-form", {
          id: req.params.id,
          token
        });
      } catch (err) {
        return res.status(401).json({
          error: "Invalid token"
        });
      }
    }
  } catch (err) {
    return res.status(401).json({
      error: "Authorization failed"
    });
  }
};
