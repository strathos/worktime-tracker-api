"use strict";
const config = require("../config");
const express = require("express");
const app = express();
const path = require('path');
const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const Promise = require("bluebird");
const favicon = require("serve-favicon");
const cors = require("./middleware/cors");
const routeNotFound = require("./middleware/route-not-found");
const errorHandler = require("./middleware/error-handler");
const dayRoutes = require("./routes/days");
const userRoutes = require("./routes/users");

if (config.env === "test") {
  const { MongoMemoryServer } = require("mongodb-memory-server");
  const mongoServer = new MongoMemoryServer();
  mongoServer.getConnectionString().then((mongoUri) => {
    mongoose.connect(mongoUri, {
      useCreateIndex: true,
      useNewUrlParser: true,
      useFindAndModify: false
    });
    mongoose.connection.once("open", () => {
      console.log(`Connected to local test DB ${mongoUri}`);
    });
  });
} else {
  mongoose.connect(config.mongodb.uri, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false
  });
  mongoose.connection.once("open", () => {
    console.log("Connected to MongoDB");
  });
}

mongoose.Promise = Promise;

app.use(morgan("dev"));
app.set("views", path.join(__dirname, "/views"));
app.set("view engine", "pug");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors);

app.use("/static", express.static(path.join(__dirname, "static")));
app.use(favicon(path.join(__dirname, "static", "favicon.ico")));
app.use("/days", dayRoutes);
app.use("/user", userRoutes);

app.use(routeNotFound);
app.use(errorHandler);

module.exports = app;
