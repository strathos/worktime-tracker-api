"use strict";
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const User = require("../models/user");

module.exports = async (req, res) => {
  try {
    const user = await User.findOne({ _id: req.params.id }).exec();
    if (user == null) {
      return res.status(401).json({
        error: "Authorization failed"
      });
    } else {
      const token = req.params.token;
      try {
        await jwt.verify(token, user.password);
        if (req.body.newPasswordFirst === req.body.newPasswordSecond) {
          bcrypt.hash(req.body.newPasswordFirst, 10, async (err, hash) => {
            if (err) {
              return res.status(500).json({
                error: err
              });
            } else {
              try {
                await User.findOneAndUpdate({ _id: req.params.id }, { $set: { password: hash } }).exec();
                return res.status(200).json({
                  message: "Password set"
                });
              } catch (err) {
                console.error(err);
                res.status(500).json({
                  error: err
                });
              }
            }
          });
        } else {
          return res.status(401).json({
            error: "Passwords don't match"
          });
        }
      } catch (err) {
        return res.status(401).json({
          error: "Invalid token"
        });
      }
    }
  } catch (err) {
    return res.status(401).json({
      error: "Authorization failed"
    });
  }
};
